# NTP based LED Wall Clock

This is a simple LED wall clock based on a [Teensy 4.1](https://www.pjrc.com/store/teensy41.html) from PJRC. 

![WallClock](img/NTPWallClock.jpg "NTP Wall Clock")

## Hardware:
* [Teensy 4.1](https://www.pjrc.com/store/teensy41.html) with the Ethernet Kit
* Four 4-Digit 1.2" LED modules with I2C backpack from [Adafruit](https://www.adafruit.com/product/1270)

The LED modules are daisychained behind each other and connected to the teensy. Be careful, since the pin on the teensy are NOT 5V tolerant, connect the 5V from the Teensy board to the 5V of the LED modules and connect the 3V to the "V_IC" pin of the modules. With that you have 3V on the I2C Pins and 5V for the LED's. Otherwise you will burn your teensy.

For the I2C pin i used pin 18 an 19 on the [Teensy 4.1](https://www.pjrc.com/teensy/card11a_rev2_web.pdf)

## Software:
The softwate environment is not the traditional Arduino IDE. One of the users from the PJRC forum created a tool called [VisualTeensy](https://github.com/luni64/VisualTeensy). With this tool you can create a more or less independent environment to use Visual Studio Code from Microsoft to develeop for the Teensy 4.1 (and other Teensys)

To use that tool, you first have to create a fresh Arduino installation. Then add the [Teensyduino](https://www.pjrc.com/teensy/td_download.html) addon.
Now you can use the VisualTeensy tool to create a Makefile based project for the Teensy 4.X. And, also important, i don't use this "unholly" Setup()/Loop() stuff from Arduino, i have a traditional C style setup with main(). So don't be surprised...

To be totaly independent from the Arduino stuff, i extracted the ARM toolchain and the teensy tools and put them in a directory above the actual one called "toolchain". You have either to do the same or adept the makefile for you

So my dir structure is as follows:
* Teensy_stuff/
    * VisualTeensy/
    * toolchain/
    * teensy-4.1-based-ntp-wall-clock/

Have fun
