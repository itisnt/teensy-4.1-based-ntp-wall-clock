// =================================================================================================
// NTP Time based Wall Clock 
// =================================================================================================
// Hw: Teensy 4.1 (https://www.pjrc.com/store/teensy41.html)
//     4 x 1.2" LED Displays from Adafruit (https://www.adafruit.com/product/1270)
//     Coin Battery for the RTC of the Teensy 4.1
// =================================================================================================
// 30-Jan-2021 bue Firstlight 
// 09-Apr-2021 bue Summmertime activated
// =================================================================================================

#include <Arduino.h>
#include <SPI.h>
#include <NativeEthernet.h>
#include <NativeEthernetUdp.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_LEDBackpack.h>
#include <TimeLib.h>
#include "datetime.h"

extern "C"  {
void summertime( volatile DATETIME_T *);
void gettime( time_t, volatile DATETIME_T *);
time_t mktime( DATETIME_T *);
}


// =================================================================================================
// Variables
// =================================================================================================

int led = LED_BUILTIN;

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

unsigned int localPort = 8888; // local port to listen for UDP packets

// const char timeServer[] = "0.ch.pool.ntp.org"; // time.nist.gov NTP server
// Does not work with the FQDN of my firewall, get too much names from getHostByName()
// So working with IP-Address
const IPAddress timeServer(192,168,1,51);

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

// A UDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

// LED
Adafruit_7segment matrix1 = Adafruit_7segment();
Adafruit_7segment matrix2 = Adafruit_7segment();
Adafruit_7segment matrix3 = Adafruit_7segment();
Adafruit_7segment matrix4 = Adafruit_7segment();

const int timeZone = 1;     // Central European Time
//const int timeZone = -5;  // Eastern Standard Time (USA)
//const int timeZone = -4;  // Eastern Daylight Time (USA)
//const int timeZone = -8;  // Pacific Standard Time (USA)
//const int timeZone = -7;  // Pacific Daylight Time (USA)

// =================================================================================================
// Declarations
// =================================================================================================

void sendNTPpacket(const IPAddress);
void processPacket(void);
time_t getTeensy3Time();
void displayTime(void);

// =================================================================================================
// Main()
// =================================================================================================

extern "C" int main(void)
{
  uint32_t elapsed_sec = 0;
  uint32_t elapsed_min = 0;

  // ********************************
  // Setup
  // ********************************

  // Set callback function for the TimeLib functions
  setSyncProvider(getTeensy3Time);

  // Heartbeat LED on the Teensy 4.1
  pinMode(led, OUTPUT);

  // Initialize all the four LED modules
  matrix1.begin(0x70);
  matrix2.begin(0x71);
  matrix3.begin(0x75);
  matrix4.begin(0x73);

  // They are very bright, so set brightness to lowest level (0 - 15)
  matrix1.setBrightness(0);
  matrix2.setBrightness(0);
  matrix3.setBrightness(0);
  matrix4.setBrightness(0);

  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("\nNTP Wall Clock starting V 1.1");

  // Fire up Ethernet and try to get a DHCP address
  if (Ethernet.begin(mac) == 0)
  {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware)
    {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    }
    else if (Ethernet.linkStatus() == LinkOFF)
    {
      Serial.println("Ethernet cable is not connected.");
    }
    // no point in carrying on, so do nothing forevermore:
    while (true)
    {
      delay(1);
    }
  }
  else
  {
    IPAddress myIP = Ethernet.localIP();
    Serial.printf("IP Address %u.%u.%u.%u\n", myIP[0], myIP[1], myIP[2], myIP[3]);
  }

  Udp.begin(localPort);

  // Start the first NTP request 
  sendNTPpacket(timeServer);

  // ********************************
  // Endless Loop
  // ********************************

  for (;;)
  {

    // One Second Loop
    if ((millis() - elapsed_sec) > 1000)
    {
      digitalToggleFast(led);
      elapsed_sec = millis();
      displayTime();
    }

    // One minute Loop
    if ((millis() - elapsed_min) > 60000)
    {
      // Send a NTP request every minute. 
      sendNTPpacket(timeServer);
      elapsed_min = millis();
    }

    // Check if we get a UDP packet, must be a NTP awnser...
    if (Udp.parsePacket())
    {
      Serial.println("Process NTP Packet");
      processPacket();
    }

    // Ethernet housekeeping
    Ethernet.maintain();
  }
}

// -------------------------------------------------------------------------- 
// Process a UDP NTP packet and extract the time
// -------------------------------------------------------------------------- 

void processPacket(void)
{
  DATETIME_T rtc_time;

  Udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

  // the timestamp starts at byte 40 of the received packet and is four bytes,
  // or two words, long. First, extract the two words:
  unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
  unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
  // combine the four bytes (two words) into a long integer
  // this is NTP time (seconds since Jan 1 1900):
  unsigned long secsSince1900 = highWord << 16 | lowWord;
  // Serial.print("Seconds since Jan 1 1900 = ");
  // Serial.println(secsSince1900);

  // NTP delivers seconds since 1900, but we need seconds since 1970
  // 70 years = 2208988800 seconds
  const unsigned long seventyYears = 2208988800UL;
  unsigned long epoch = secsSince1900 - seventyYears;

  // Timezone correction
  epoch = epoch + (timeZone * SECS_PER_HOUR);

  // 30-Jan-2021, swiched to the Arduino TimeLib, so don't use my own routines anymore
  // now convert NTP time into everyday time:
  // Convert Unix Timestamp to time struct (dateime.h)
  gettime(epoch, &rtc_time);
  // Ueberpruefen und ev. auf Sommerzeit korrigieren.
  summertime(&rtc_time);

  // Set the RTC (Name of function is confusing, but works for Teensy 4.1)
  Teensy3Clock.set(mktime(&rtc_time));

  // print the hour, minute and second:
  Serial.printf("Time: %02d.%02d.%04d %02d:%02d:%02d\n", rtc_time.day, rtc_time.month, rtc_time.year, rtc_time.hour, rtc_time.minute, rtc_time.second);
}

// -------------------------------------------------------------------------- 
// Send an NTP request to the time server at the given address
// -------------------------------------------------------------------------- 

void sendNTPpacket(const IPAddress address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011; // LI, Version, Mode
  packetBuffer[1] = 0;          // Stratum, or type of clock
  packetBuffer[2] = 6;          // Polling Interval
  packetBuffer[3] = 0xEC;       // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;

  Serial.print("Start NTP Request.....");
  Udp.beginPacket(address, 123); // NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
  Serial.println("Done");
}

// -------------------------------------------------------------------------- 
// Display Time / Date
// -------------------------------------------------------------------------- 
// Lower right: matrix1
// Lower left : matrix2
// Upper left : matrix3 
// Upper right: matrix4
// -------------------------------------------------------------------------- 

void displayTime(void)
{
  matrix3.writeDigitNum(0, (hour() / 10));
  matrix3.writeDigitNum(1, (hour() % 10));
  matrix3.writeDigitNum(3, (minute() / 10));
  matrix3.writeDigitNum(4, (minute() % 10));
  matrix3.writeDigitRaw(2,0x02); // The middle colon are connected together 
  matrix3.writeDisplay();

  matrix4.writeDigitNum(0, (second() / 10));
  matrix4.writeDigitNum(1, (second() % 10));
  matrix4.writeDigitRaw(2,0x0C); // Switches both colon on the left on
  matrix4.writeDisplay();

  matrix2.writeDigitNum(0, (day() / 10));
  matrix2.writeDigitNum(1, (day() % 10));
  matrix2.writeDigitNum(3, (month() / 10));
  matrix2.writeDigitNum(4, (month() % 10));
  matrix2.writeDigitRaw(2,0x02); 
  matrix2.writeDisplay();

  matrix1.writeDigitNum(0, (year() / 1000));
  matrix1.writeDigitNum(1, (( year() / 100)) % 10);
  matrix1.writeDigitNum(3, (( year() / 10)) % 10);
  matrix1.writeDigitNum(4, (year() % 10));
  // matrix1.writeDigitRaw(2,0x0C); // No colon in the year ;-)
  matrix1.writeDigitRaw(2,0x08); // Only the lower colon on the left
  matrix1.writeDisplay();
}

// -------------------------------------------------------------------------- 
// Call back funciton for the TimeLib functions. This is called in now() every time the 
// the time is updated. Could also be a NTPget function, but i  think this would block
// the program
// -------------------------------------------------------------------------- 

time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}