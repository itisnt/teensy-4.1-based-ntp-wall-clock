#ifndef DATETIME_H
#define DATETIME_H
//************************************************************************
//*
//*			Convert 32 bit to date & time
//*			e.g. from RTC chip DS1994
//*
//*		Author: Peter Dannegger
//*     			danni@specs.de
//*
//************************************************************************

// 4294967295 sec = 0136 y + 1 m + 6 d + 6 h + 28 min + 15 sec

// 1.1.1970 war ein Donnerstag
#define FIRSTYEAR	1970		// start year
#define FIRSTDAY	4			// 0 = Sunday

typedef struct
{
  unsigned int   second;
  unsigned int   minute;
  unsigned int   hour;
  unsigned int   day;
  unsigned int   month;
  unsigned short year;
  unsigned int   wday;
} DATETIME_T;

// Nicht noetig in einem Arduino Program
// typedef unsigned long time_t;
// Muessen im main.cpp als .... extern "C".... deklariert werden
//void summertime( volatile DATETIME_T *);
//void gettime( time_t, volatile DATETIME_T *);
// time_t mktime( DATETIME_T *);
unsigned int isLeapYear( unsigned int);


volatile DATETIME_T rtc_time;
#endif