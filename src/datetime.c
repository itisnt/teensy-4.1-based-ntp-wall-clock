//************************************************************************
//*
//*			Convert 32 bit to date & time
//*			e.g. from RTC chip DS1994
//*
//*		Author: Peter Dannegger
//*     			danni@specs.de
//*
//************************************************************************

#include <stdio.h>
#include "datetime.h"

const unsigned int DayOfMonth[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

// Zum testen:
//
// int main(void)
// {
// 	time_t sec = 3426186697 - 2208988800;
// 	time_t ret_sec = 0;
// 
// 	gettime(sec, &rtc_time);
// 
// 	printf("%02d.%02d.%04d %02d:%02d:%02d\n",rtc_time.day, rtc_time.month, rtc_time.year, rtc_time.hour, rtc_time.minute, rtc_time.second);
// 
// 	ret_sec = mktime(&rtc_time);
// 
// 	printf("old: %lu  new: %lu\n",sec, ret_sec);
// }

//************************************************************************
//* Check ob Sommerzeit oder nicht
//************************************************************************

void summertime( volatile DATETIME_T *t )
{
  unsigned int hour, day, wday, month;			// locals for faster access

  hour = t->hour;
  day = t->day;
  wday = t->wday;
  month = t->month;

  if( month < 3 || month > 10 )			// month 1, 2, 11, 12
    return;					// -> Winter

  if( day - wday >= 25 && (wday || hour >= 2) ){ // after last Sunday 2:00
    if( month == 10 )				// October -> Winter
      return;
  }else{					// before last Sunday 2:00
    if( month == 3 )				// March -> Winter
      return;
  }
  hour++;					// add one hour
  if( hour == 24 ){				// next day
    hour = 0;
    wday++;					// next weekday
    if( wday == 7 )
      wday = 0;
    if( day == DayOfMonth[month-1] ){		// next month
      day = 0;
      month++;
    }
    day++;
  }
  t->month = month;
  t->hour = hour;
  t->day = day;
  t->wday = wday;
}

//************************************************************************
//* Umwandeln Timestamp in Datumsstruktur
//************************************************************************

void gettime( time_t sec, volatile DATETIME_T *t )
{
  unsigned long day;
  unsigned int year;
  unsigned short dayofyear;
  unsigned int leap400;
  unsigned int month;
  // unsigned long t_sec;
  // t_sec = sec;

  t->second = (sec % 60);
  t->minute = (sec / 60) % 60;
  t->hour   = (sec / 3600) % 24;
  day = (sec / 3600 ) / 24;

  t->wday = (day + FIRSTDAY) % 7;		// weekday

  year = FIRSTYEAR % 100;			// 0..99

  leap400 = 4 - ((FIRSTYEAR - 1) / 100 & 3);	// 4, 3, 2, 1

  for(;;)
  {
    dayofyear = 365;
    if( (year & 3) == 0 )
	{
      dayofyear = 366;					// leap year
      if( year == 0 || year == 100 || year == 200 )	// 100 year exception
	  {
        if( --leap400 )					// 400 year exception
		{
          dayofyear = 365;
		}
	  }
    }
    if( day < dayofyear ) break;
    day -= dayofyear;
    year++;					// 00..136 / 99..235
  }
 
  t->year = year + FIRSTYEAR / 100 * 100;	// + century

  if( dayofyear & 1 && day > 58 )		// no leap year and after 28.2.
    day++;					// skip 29.2.

  for( month = 1; day >= DayOfMonth[month-1]; month++ )
  {
    day -= DayOfMonth[month-1];
  }

  t->month = month;				// 1..12
  t->day = day + 1;				// 1..31

}

//************************************************************************
//* Umwandeln Datumsstruktur in Timestamp
//************************************************************************

time_t mktime(DATETIME_T *timeptr)
{
	int year=timeptr->year;
	int month=timeptr->month;
	int i;
	long seconds;
 
 	// Sekunden vom 1.1.1970 00:00:00 bis zum 1.Jan des angegebenen Jahres 00:00:00
	
	seconds= (year-1970)*(60*60*24L*365);

	// Fuer jedes Schaltjahr einen Tag dazuzaehlen

	for (i=1970; i<year; i++) {
		if (isLeapYear(i)) {
			seconds+= 60*60*24L;
		}
	}

	// Die restlichen Tage dieses Jahres addieren, ohne aktuellen Monat

	for (i=0; i<(month-1); i++) {
		if (i==1 && isLeapYear(year)) { 
			seconds+= 60*60*24L*29;
		} else {
			seconds+= 60*60*24L*DayOfMonth[i];
		}
	}

    // Die restlichen Tage des aktuellen Monats addieren, ohne den aktuellen Tag
	seconds+= (timeptr->day-1)*60*60*24L;
	// Stunden, Minuten und Sekunden des aktuellen Tages addieren
	seconds+= timeptr->hour*60*60;
	seconds+= timeptr->minute*60;
	seconds+= timeptr->second;
	return seconds;
}

//************************************************************************
//* Bestimmen, ob ein Jahr ein Schaltjahr ist
//************************************************************************

unsigned int isLeapYear( unsigned int year )
{
	return( ( ( ( year % 4 ) == 0 ) &&
			( ( year % 100 ) != 0 ) )
			|| ( ( year % 400 ) == 0 ) );
}

